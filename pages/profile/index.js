import { Container, Jumbotron } from "react-bootstrap";

export default function Profile() {
  return (
    <>
      <Jumbotron>
        <Container className="d-flex flex-column align-items-center">
          <h1>Developers</h1>
          <div id="kim" className="mt-4" />
          <h2>Kim Buenaobra</h2>
          <p>
            A graphic artist that is transition from making social media
            materials into creating beautiful websites. His man goal is to help
            small business owners into making their idea into a reality.
          </p>
        </Container>
      </Jumbotron>
      <Jumbotron>
        <Container className="d-flex flex-column align-items-center">
          <div id="jerwin" />
          <h2>Jerwin Babatugon</h2>
          <p>
            My name is Jerwin Babatugon. I am a coffee lover and can't live
            without it. I have enjoyed very much working as a programmer and
            love to provide assistance to the companies that need my help.
          </p>
        </Container>
      </Jumbotron>
    </>
  );
}
