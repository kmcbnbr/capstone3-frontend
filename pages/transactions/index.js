import { Container, Table, Button, Form, Col } from "react-bootstrap";
import { useState, useContext } from "react";
import NewTransactionModal from "../../components/NewTransactionModal";
import UserContext from "../../UserContext";
import moment from "moment";
import AppHelper from "../../app_helper";

export default function Transactions() {
  const { user, setUser } = useContext(UserContext);
  const [visible, setVisible] = useState(false);
  const [typeFilter, setTypeFilter] = useState("Filter by type");
  const [search, setSearch] = useState("");
  let budget = 0;
  user.records.forEach((record) => {
    if (record.categoryType == "Income") {
      budget += record.amount;
    } else {
      budget -= record.amount;
    }
  });

  let records = user.records;
  if (typeFilter === "Expense")
    records = records.filter((record) => record.categoryType === "Expense");
  else if (typeFilter === "Income")
    records = records.filter((record) => record.categoryType === "Income");

  const searchTerm = search.trim();
  if (searchTerm) {
    records = records.filter((record) =>
      `${record.categoryName} ${record.description}`
        .toLowerCase()
        .includes(searchTerm.toLowerCase())
    );
  }

  return (
    <Container className="d-flex flex-column align-items-center">
      <div className="bg-dark mt-5">
        <h3 className="p-4 text-white text-center">
          Welcome back, {user.firstName} {user.lastName}!
        </h3>
      </div>
      <h4 className="text-center mt-2">
        You have {moment().daysInMonth() - moment().date()} days left for the
        month of {moment().format("MMMM YYYY")}.
      </h4>
      <h3 className="text-center mt-2">
        Budget Remaining: {AppHelper.formatAmount(budget)}
      </h3>
      <Container className="mt-5 d-flex">
        <Col xs={2} md={4} auto>
          <Button
            className="rounded"
            variant="success"
            onClick={() => setVisible(true)}
          >
            Add Transaction
          </Button>
        </Col>
        <Col xs={2} md={4} auto>
          <Form.Control
            type="text"
            placeholder="Search transactions..."
            value={search}
            onChange={(e) => {
              setSearch(e.target.value);
            }}
          />
        </Col>
        <Col xs={2} md={4} auto>
          <Form.Control
            as="select"
            value={typeFilter}
            onChange={(e) => {
              setTypeFilter(e.target.value);
            }}
          >
            <option>Filter by type</option>
            <option>Expense</option>
            <option>Income</option>
          </Form.Control>
        </Col>
      </Container>
      <Table className="mt-5">
        <thead>
          <tr>
            <th>Date</th>
            <th>Category</th>
            <th>Type</th>
            <th>Amount</th>
            <th>Remarks</th>
          </tr>
        </thead>
        <tbody>
          {records.map((record, i) => (
            <tr key={i}>
              <td>{record.inputOn}</td>
              <td>{record.categoryName}</td>
              <td>{record.categoryType}</td>
              <td>{AppHelper.formatAmount(record.amount)}</td>
              <td>{record.description}</td>
            </tr>
          ))}
        </tbody>
      </Table>
      <NewTransactionModal
        visible={visible}
        onHide={() => setVisible(false)}
        onSave={() => setVisible(false)}
      />
    </Container>
  );
}
