import { Container, Table, Button, Form } from "react-bootstrap";
import { useState, useContext } from "react";
import NewCategoryModal from "../../components/NewCategoryModal";
import UserContext from "../../UserContext";

export default function Categories() {
  const [visible, setVisible] = useState(false);
  const { user, setUser } = useContext(UserContext);
  const [search, setSearch] = useState("");
  let categories = user.categories;
  const searchTerm = search.trim();
  if (searchTerm) {
    categories = categories.filter((category) =>
      `${category.categoryName} ${category.categoryType}`
        .toLowerCase()
        .includes(searchTerm.toLowerCase())
    );
  }
  return (
    <Container className="d-flex flex-column align-items-center">
      <div className="bg-dark mt-5">
        <h3 className="p-4 text-white text-center">Categories</h3>
      </div>
      <div className="mt-5 align-self-stretch d-flex justify-content-between">
        <Button
          className="rounded"
          variant="success"
          onClick={() => setVisible(true)}
        >
          Add Category
        </Button>
        <Form className="d-flex">
          <Form.Control
            type="text"
            placeholder="Search categories..."
            value={search}
            onChange={(e) => {
              setSearch(e.target.value);
            }}
          />
        </Form>
      </div>
      <Table className="mt-5">
        <thead>
          <tr>
            <th>Category</th>
            <th>Type</th>
          </tr>
        </thead>
        <tbody>
          {categories.map((category) => (
            <tr key={category.categoryName}>
              <td>{category.categoryName}</td>
              <td>{category.categoryType}</td>
            </tr>
          ))}
        </tbody>
      </Table>
      <NewCategoryModal visible={visible} onHide={() => setVisible(false)} />
    </Container>
  );
}
