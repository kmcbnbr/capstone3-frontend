import MonthlyExpense from "../../components/MonthlyExpense";
import MonthlyIncome from "../../components/MonthlyIncome";
import { Container } from "react-bootstrap";

export default function Reports() {
  return (
    <Container>
      <MonthlyExpense />
      <MonthlyIncome />
    </Container>
  );
}
