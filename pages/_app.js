import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/globals.css";
import NavBar from "../components/NavBar";
import { UserProvider } from "../UserContext";
import { useState, useEffect } from "react";
import AppHelper from "../app_helper";

const initialUser = {
  id: null,
  firstName: null,
  lastName: null,
  email: null,
  categories: [],
  records: [],
};

function MyApp({ Component, pageProps }) {
  const [user, setUser] = useState(initialUser);
  useEffect(() => {
    fetch(`${AppHelper.API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({
          id: data._id,
          firstName: data.firstName,
          lastName: data.lastName,
          email: data.email,
          categories: data.categories,
          records: data.records,
        });
      });
  }, []);
  const unsetUser = () => {
    localStorage.clear();
    setUser(initialUser);
  };
  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      {user.id ? <NavBar /> : null}
      <Component {...pageProps} />
    </UserProvider>
  );
}

export default MyApp;
