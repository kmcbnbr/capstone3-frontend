import UserContext from "../../UserContext";
import { useContext, useEffect } from "react";
import Router from "next/router";

export default function Logout() {
  const { unsetUser } = useContext(UserContext);
  useEffect(() => {
    unsetUser();
    Router.replace("/");
  }, []);
  return null;
}
