import { GoogleLogin } from "react-google-login";
import Swal from "sweetalert2";
import Router from "next/router";
import AppHelper from "../app_helper";
import { useContext, useEffect, useState } from "react";
import { Form, Button, Modal } from "react-bootstrap";
import UserContext from "../UserContext";
import SignUpModal from "../components/SignUpModal";

export default function Home() {
  const { user, setUser } = useContext(UserContext);
  const [visible, setVisible] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  function authenticateGoogleToken(response) {
    const payload = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        tokenId: response.tokenId,
        accessToken: response.accessToken,
      }),
    };
    fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, payload)
      .then(AppHelper.toJSON)
      .then((data) => {
        if (data.accessToken) {
          localStorage.setItem("token", data.accessToken);
          fetch(`${AppHelper.API_URL}/users/details`, {
            headers: {
              Authorization: `Bearer ${AppHelper.getAccessToken()}`,
            },
          })
            .then(AppHelper.toJSON)
            .then((data) => {
              setUser({
                id: data._id,
                firstName: data.firstName,
                lastName: data.lastName,
                email: data.email,
                categories: data.categories,
                records: data.records,
              });
              Swal.fire({
                icon: "success",
                title: "Successful Login",
              });
            });
        } else {
          if (data.error == "google-auth-error") {
            Swal.fire({
              icon: "error",
              title: "Google Authentication Failed",
            });
          }
        }
      });
  }

  useEffect(() => {
    if (user.id) Router.replace("/transactions");
  }, [user]);

  const handleLogin = (e) => {
    e.preventDefault();
    const payload = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    };
    fetch(`${AppHelper.API_URL}/users/login`, payload)
      .then(AppHelper.toJSON)
      .then((data) => {
        if (data.accessToken) {
          localStorage.setItem("token", data.accessToken);
          fetch(`${AppHelper.API_URL}/users/details`, {
            headers: {
              Authorization: `Bearer ${AppHelper.getAccessToken()}`,
            },
          })
            .then(AppHelper.toJSON)
            .then((data) => {
              setUser({
                id: data._id,
                firstName: data.firstName,
                lastName: data.lastName,
                email: data.email,
                categories: data.categories,
                records: data.records,
              });
              Swal.fire({
                icon: "success",
                title: "Successful Login",
              });
            });
        } else {
          Swal.fire({
            icon: "error",
            title: "Login Failed",
          });
        }
      });
  };
  return (
    <div
      className="h-100 d-flex align-items-center justify-content-center"
      id="home"
    >
      <div
        id="home-card"
        className="p-5 d-flex flex-column align-items-center bg-white w-100 shadow-lg"
      >
        <h1>KWENTA</h1>
        <h4>Budget Tracker</h4>
        <h3 className="mt-2">Spend your money wisely.</h3>
        <Form className="mt-2 w-100" onSubmit={handleLogin}>
          <Form.Group>
            <Form.Label>Email Address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email address here"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Input valid password here"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </Form.Group>
          <Button block variant="success" type="onSubmit">
            Log in
          </Button>
        </Form>
        <Button block className="mt-4" onClick={() => setVisible(true)}>
          Sign Up
        </Button>
        <GoogleLogin
          clientId="38077020784-v9t8p0lrj6qllvfadttb69qu9sic2ag4.apps.googleusercontent.com"
          buttonText="Login Using Google"
          cookiesPolicy={"single_host_origin"}
          onSuccess={authenticateGoogleToken}
          onFailure={authenticateGoogleToken}
          className="mt-4 w-100 justify-content-center"
        />
      </div>
      <SignUpModal visible={visible} onHide={() => setVisible(false)} />
    </div>
  );
}
