import { Nav, Navbar } from "react-bootstrap";

import Link from "next/link";

export default function NavBar() {
  return (
    <Navbar bg="light" expand="lg">
      <Link href="/transactions" passHref>
        <Navbar.Brand>Kwenta - Budget Tracker</Navbar.Brand>
      </Link>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Link href="/transactions">
            <a className="nav-link" role="button">
              Transactions
            </a>
          </Link>
          <Link href="/reports">
            <a className="nav-link" role="button">
              Reports
            </a>
          </Link>
          <Link href="/categories">
            <a className="nav-link" role="button">
              Categories
            </a>
          </Link>
          <Link href="/profile">
            <a className="nav-link" role="button">
              Developer's Profile
            </a>
          </Link>
          <Link href="/logout">
            <a className="nav-link" role="button">
              Logout
            </a>
          </Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
