import { Modal, Form, Button } from "react-bootstrap";
import { useContext, useState } from "react";
import PropTypes from "prop-types";
import AppHelper from "../app_helper";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function NewCategoryModal(props) {
  const { onHide, visible } = props;
  const [categoryName, setCategoryName] = useState("");
  const [categoryType, setCategoryType] = useState("Expense");

  const { setUser } = useContext(UserContext);

  function addCategory() {
    const payload = {
      method: "POST",
      headers: {
        Authorization: `Bearer ${AppHelper.getAccessToken()}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        categoryName: categoryName,
        categoryType: categoryType,
      }),
    };
    fetch(`${AppHelper.API_URL}/users/new-category`, payload)
      .then(AppHelper.toJSON)
      .then(() => {
        fetch(`${AppHelper.API_URL}/users/details`, {
          headers: {
            Authorization: `Bearer ${AppHelper.getAccessToken()}`,
          },
        })
          .then(AppHelper.toJSON)
          .then((data) => {
            setUser({
              id: data._id,
              firstName: data.firstName,
              lastName: data.lastName,
              email: data.email,
              categories: data.categories,
              records: data.records,
            });
            Swal.fire({
              icon: "success",
              title: "Category added!",
            });
            onHide();
          });
      });
  }

  return (
    <Modal size="lg" centered show={visible}>
      <Modal.Header closeButton onHide={onHide}>
        <Modal.Title>Add Category</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group>
            <Form.Label>Category</Form.Label>
            <Form.Control
              type="text"
              placeholder="e.g. Food, Travel"
              value={categoryName}
              onChange={(e) => setCategoryName(e.target.value)}
              required
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Type</Form.Label>
            <Form.Control
              as="select"
              value={categoryType}
              onChange={(e) => setCategoryType(e.target.value)}
              required
            >
              <option>Expense</option>
              <option>Income</option>
            </Form.Control>
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={addCategory}>Save</Button>
      </Modal.Footer>
    </Modal>
  );
}

NewCategoryModal.propTypes = {
  onHide: PropTypes.func.isRequired,
  visible: PropTypes.bool.isRequired,
};
