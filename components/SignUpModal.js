import { Modal, Form, Button } from "react-bootstrap";
import { useContext, useState } from "react";
import PropTypes from "prop-types";
import AppHelper from "../app_helper";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function SignUpModal(props) {
  const { onHide, visible } = props;
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { setUser } = useContext(UserContext);

  const handleSignUp = (e) => {
    e.preventDefault();
    const payload = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password,
      }),
    };
    fetch(`${AppHelper.API_URL}/users/`, payload)
      .then(AppHelper.toJSON)
      .then((data) => {
        if (data) {
          const payload = {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
              email: email,
              password: password,
            }),
          };
          fetch(`${AppHelper.API_URL}/users/login`, payload)
            .then(AppHelper.toJSON)
            .then((data) => {
              if (data.accessToken) {
                localStorage.setItem("token", data.accessToken);
                fetch(`${AppHelper.API_URL}/users/details`, {
                  headers: {
                    Authorization: `Bearer ${AppHelper.getAccessToken()}`,
                  },
                })
                  .then(AppHelper.toJSON)
                  .then((data) => {
                    setUser({
                      id: data._id,
                      firstName: data.firstName,
                      lastName: data.lastName,
                      email: data.email,
                      categories: data.categories,
                      records: data.records,
                    });
                    Swal.fire({
                      icon: "success",
                      title: "Successful Sign Up",
                    });
                  });
              }
            });
        } else {
          Swal.fire({
            icon: "error",
            title: "Sign up failed",
          });
        }
      });
  };

  return (
    <Modal size="lg" centered show={visible}>
      <Modal.Header closeButton onHide={onHide}>
        <Modal.Title>Sign Up</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form onSubmit={handleSignUp}>
          <Form.Group>
            <Form.Label>First Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Put first name here"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
              required
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Last Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Put last name here"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
              required
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Email Address</Form.Label>
            <Form.Control
              type="text"
              placeholder="Put email here"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Put password here"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </Form.Group>
          <Button type="submit">Sign Up</Button>
        </Form>
      </Modal.Body>
    </Modal>
  );
}

SignUpModal.propTypes = {
  onHide: PropTypes.func.isRequired,
  visible: PropTypes.bool.isRequired,
};
