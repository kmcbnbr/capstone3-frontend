import { Modal, Form, Button, Container } from "react-bootstrap";
import PropTypes from "prop-types";
import { useState, useContext } from "react";
import AppHelper from "../app_helper";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function NewTransactionModal(props) {
  const { onHide, visible } = props;

  const { user, setUser } = useContext(UserContext);
  const [category, setCategory] = useState("");
  const [type, setType] = useState("");
  const [amount, setAmount] = useState(0);
  const [remarks, setRemarks] = useState("");

  function addTransaction() {
    const [categoryType, categoryName] = category.split(" - ");
    const payload = {
      method: "POST",
      headers: {
        Authorization: `Bearer ${AppHelper.getAccessToken()}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        categoryName: categoryName,
        categoryType: categoryType,
        amount: amount,
        description: remarks,
      }),
    };
    fetch(`${AppHelper.API_URL}/users/new-record`, payload)
      .then(AppHelper.toJSON)
      .then(() => {
        fetch(`${AppHelper.API_URL}/users/details`, {
          headers: {
            Authorization: `Bearer ${AppHelper.getAccessToken()}`,
          },
        })
          .then(AppHelper.toJSON)
          .then((data) => {
            setUser({
              id: data._id,
              firstName: data.firstName,
              lastName: data.lastName,
              email: data.email,
              categories: data.categories,
              records: data.records,
            });
            Swal.fire({
              icon: "success",
              title: "Transaction added!",
            });
            onHide();
          });
      });
  }
  return (
    <Modal size="lg" centered show={visible}>
      <Modal.Header closeButton onHide={onHide}>
        <Modal.Title>Add Transaction</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group>
            <Form.Label>Category</Form.Label>
            <Form.Control
              as="select"
              value={category}
              onChange={(e) => setCategory(e.target.value)}
              required
            >
              {user.categories.map((category, i) => (
                <option key={i}>
                  {category.categoryType} - {category.categoryName}
                </option>
              ))}
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>Amount</Form.Label>
            <Form.Control
              type="number"
              placeholder="Input amount"
              value={amount}
              onChange={(e) => setAmount(e.target.value)}
              required
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Remarks</Form.Label>
            <Form.Control
              type="text"
              placeholder="Input remarks"
              value={remarks}
              onChange={(e) => setRemarks(e.target.value)}
              required
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={addTransaction}>Save</Button>
      </Modal.Footer>
    </Modal>
  );
}

NewTransactionModal.propTypes = {
  onHide: PropTypes.func.isRequired,
  visible: PropTypes.bool.isRequired,
  onSave: PropTypes.func.isRequired,
};
